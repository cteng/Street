﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class citycreat : MonoBehaviour
{
	public Transform[] block;
	public GameObject[] roads;
	public GameObject[] fences;
	public GameObject[] houses;
	public GameObject lamps;
	public GameObject[] streetprops;
	public GameObject[] trafficlights;
	public GameObject test;
	public int border_r;
	public int border_l;
	public int border_u;
	public int border_b;
	public int location;
	public int mapwidth = 10;
	public int mapheight = 10;
	public int widthi = 0;
	public int heighti = 0;
	public int corner = 0;
	public int props = 0;
	public int[,] maps;
	public int obSize = 30;
	public int r = 0;
	public bool[] row1;
	public bool[] row2;
	public bool[] row3;
	public bool[] column1;
	public bool[] column2;
	public bool[] column3;
	
	
	//produce a block
	void build(int block_num, int x, int z)
	{
		//maps = new int[mapwidth, mapheight];
	
		//try block
		block[block_num].position = new Vector3(x,0,z);
		
		//angle of road
		Quaternion roadangle_right = Quaternion.AngleAxis(-90, Vector3.right);
		Quaternion roadangle_forward = Quaternion.AngleAxis(90, Vector3.forward);
		Quaternion roadangle_c = roadangle_right * roadangle_forward;
		Quaternion po90 = Quaternion.AngleAxis(90, Vector3.up);
		Quaternion ne90 = Quaternion.AngleAxis(-90, Vector3.up);
			
		//creat roads|grass, lamp, traffic light
		for (widthi = 0; widthi < mapwidth; widthi++)
		{
			for (heighti = 0; heighti < mapheight; heighti++)
			{
				Vector3 pos = new Vector3(x + widthi*obSize, 0, z + heighti*obSize);
				Vector3 r_pos_left = new Vector3(x + widthi*obSize - 8, 0, z + heighti*obSize);
				Vector3 r_pos_right = new Vector3(x + widthi*obSize + 8, 0, z + heighti*obSize);
				Vector3 c_pos_left = new Vector3(x + widthi*obSize, 0, z + heighti*obSize - 8);
				Vector3 c_pos_right = new Vector3(x + widthi*obSize, 0, z + heighti*obSize + 8);
				
				Vector3 rightdown = new Vector3(x + widthi*obSize + 7, 0, z + heighti*obSize - 7);
				Vector3 leftup = new Vector3(x + widthi*obSize - 7, 0, z + heighti*obSize + 7);
				
				if (maps[widthi, heighti] == 8)				//cross road
				{
					Instantiate(roads[2], pos, Quaternion.identity, block[block_num].transform);
					Instantiate(roads[0], pos, roadangle_right, block[block_num].transform);
					r = Random.Range(0, 3);
					Instantiate(trafficlights[r], rightdown, Quaternion.AngleAxis(90, Vector3.left)*Quaternion.AngleAxis(90, Vector3.back), block[block_num].transform);
					r = Random.Range(0, 3);
					Instantiate(trafficlights[r], leftup, Quaternion.AngleAxis(90, Vector3.left)*Quaternion.AngleAxis(90, Vector3.forward), block[block_num].transform);
				}
				else if (maps[widthi, heighti] == 5)		//street_r
				{
					Instantiate(roads[2], pos, Quaternion.identity, block[block_num].transform);
					Instantiate(roads[1], pos, roadangle_right, block[block_num].transform);
					Instantiate(lamps, r_pos_left, Quaternion.identity, block[block_num].transform);
					Instantiate(lamps, r_pos_right, Quaternion.AngleAxis(180, Vector3.up), block[block_num].transform);
				}
				else if (maps[widthi, heighti] == 3)		//street_c
				{
					Instantiate(roads[2], pos, Quaternion.identity, block[block_num].transform);
					Instantiate(roads[1], pos, roadangle_c, block[block_num].transform);
					Instantiate(lamps, c_pos_left, ne90, block[block_num].transform);
					Instantiate(lamps, c_pos_right, po90, block[block_num].transform);
				}
				else Instantiate(roads[3], pos, Quaternion.identity, block[block_num].transform);//grass(no road)
			}
		}
		
		//creat street props
		for (widthi = 0; widthi < mapwidth; widthi++)
		{
			for (heighti = 0; heighti < mapheight; heighti++)
			{
				Vector3 rightup = new Vector3(x + widthi*obSize + 8, 0, z + heighti*obSize + 8);
				Vector3 rightdown = new Vector3(x + widthi*obSize + 8, 0, z + heighti*obSize - 8);
				Vector3 leftup = new Vector3(x + widthi*obSize - 8, 0, z + heighti*obSize + 8);
				Vector3 leftdown = new Vector3(x + widthi*obSize - 8, 0, z + heighti*obSize - 8);
				if (maps[widthi, heighti] == 5)
				{
					corner = 0;
					while (corner < 4)
					{
						r = Random.Range(0,11);
						if (r > 6)
						{
							if (r == 7) props = 0;
							else if (r == 8) props = 1;
							else if (r == 9) props = 2;
							else props = 3;

							if (corner == 0) Instantiate(streetprops[props], rightup, roadangle_right*Quaternion.identity, block[block_num].transform);
							else if (corner == 1) Instantiate(streetprops[props], rightdown, roadangle_right*Quaternion.identity, block[block_num].transform);
							else if (corner == 2) Instantiate(streetprops[props], leftup, roadangle_right*Quaternion.AngleAxis(180, Vector3.forward), block[block_num].transform);
							else Instantiate(streetprops[props], leftdown, roadangle_right*Quaternion.AngleAxis(180, Vector3.forward), block[block_num].transform);
						}
						corner++;
					}
				}
				else if (maps[widthi, heighti] == 3)
				{
					corner = 0;
					while (corner < 4)
					{
						r = Random.Range(0,11);
						if (r > 6)
						{
							if (r == 7) props = 0;
							else if (r == 8) props = 1;
							else if (r == 9) props = 2;
							else props = 3;
							
							if (corner == 0) Instantiate(streetprops[props], rightup, roadangle_right*Quaternion.AngleAxis(-90, Vector3.forward), block[block_num].transform);
							else if (corner == 1) Instantiate(streetprops[props], rightdown, roadangle_right*Quaternion.AngleAxis(90, Vector3.forward), block[block_num].transform);
							else if (corner == 2) Instantiate(streetprops[props], leftup, roadangle_right*Quaternion.AngleAxis(-90, Vector3.forward), block[block_num].transform);
							else Instantiate(streetprops[props], leftdown, roadangle_right*Quaternion.AngleAxis(90, Vector3.forward), block[block_num].transform);
						}
						corner++;
					}
				}
			}
		}
		
		//creat buildings
//		int space = obSize / 4;
		for (widthi = 0; widthi < mapwidth; widthi++)
		{
			for (heighti = 0; heighti < mapheight; heighti++)
			{
				Vector3 pos = new Vector3(x + widthi*obSize, 0, z + heighti*obSize);

				if (maps[widthi, heighti] == 0)
				{

					if (widthi > 0 && heighti > 0 && maps[widthi - 1, heighti - 1] == 8) //the houses on the rightup corner
					{
						r = Random.Range(18, 21);
						if (r < 20)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 5, 0, z + heighti * obSize - 5);
							Instantiate(houses[r], pos_house, Quaternion.identity, block[block_num].transform);
						}
						else if (r == 20)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize, 0, z + heighti * obSize);
							Instantiate(houses[r], pos_house, Quaternion.AngleAxis(-90, Vector3.right), block[block_num].transform);
						}
						else
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 10, 0, z + heighti * obSize - 10);
							Instantiate(test, pos_house, Quaternion.identity, block[block_num].transform);
						}

					}
					else if (widthi > 0 && heighti < mapheight - 1 && maps[widthi - 1, heighti + 1] == 8)
					{
						Instantiate(test, pos, Quaternion.identity, block[block_num].transform);
					}
					else if (widthi < mapwidth - 1 && heighti > 0 && maps[widthi + 1, heighti - 1] == 8)
					{
						Instantiate(test, pos, Quaternion.identity, block[block_num].transform);
					}
					else if (widthi < mapwidth - 1 && heighti < mapheight - 1 && maps[widthi + 1, heighti + 1] == 8)
					{
						Instantiate(test, pos, Quaternion.identity, block[block_num].transform);
					}
					else if (widthi > 0 && maps[widthi - 1, heighti] == 5) //the houses on the right hand side of roads
					{
						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 5, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, Quaternion.identity, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize - 12, 0, z + heighti * obSize - 15);
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, ne90, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, po90, block[block_num].transform);
						}

						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 5, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, Quaternion.identity, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize - 12, 0, z + heighti * obSize);
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, ne90, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, po90, block[block_num].transform);
						}
					}
					else if (widthi < mapwidth - 1 && maps[widthi + 1, heighti] == 5) //the houses on the left hand side of roads
					{
						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize + 5, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, Quaternion.AngleAxis(180, Vector3.up), block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize + 12, 0, z + heighti * obSize - 15);
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, po90, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, po90, block[block_num].transform);
						}

						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize + 5, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, Quaternion.AngleAxis(180, Vector3.up), block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize + 12, 0, z + heighti * obSize);
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, po90, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, po90, block[block_num].transform);
						}

					}
					else if (heighti > 0 && maps[widthi, heighti - 1] == 3) //the houses in front of the roads
					{
						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize - 5);
							Instantiate(houses[r], pos_house, ne90, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize - 2, 0, z + heighti * obSize - 12);
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, Quaternion.AngleAxis(180, Vector3.up), block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, Quaternion.identity, block[block_num].transform);
						}

						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize - 5);
							Instantiate(houses[r], pos_house, ne90, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize + 14, 0, z + heighti * obSize - 12);
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize + 8);
							Instantiate(houses[r], pos_house, Quaternion.identity, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, Quaternion.identity, block[block_num].transform);
						}


					}
					else if (heighti < mapheight - 1 && maps[widthi, heighti + 1] == 3)
					{
						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize + 5);
							Instantiate(houses[r], pos_house, po90, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize - 2, 0, z + heighti * obSize + 12);
							Vector3 pos_house = new Vector3(x + widthi * obSize - 8, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, Quaternion.identity, block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, Quaternion.identity, block[block_num].transform);
						}

						r = Random.Range(2, 17);
						if (r > 7)
						{
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize + 5);
							Instantiate(houses[r], pos_house, po90, block[block_num].transform);
						}
						else
						{
							Vector3 pos_fence = new Vector3(x + widthi * obSize + 14, 0, z + heighti * obSize + 12);
							Vector3 pos_house = new Vector3(x + widthi * obSize + 8, 0, z + heighti * obSize - 8);
							Instantiate(houses[r], pos_house, Quaternion.AngleAxis(180, Vector3.up), block[block_num].transform);
							Instantiate(fences[Random.Range(0, 1)], pos_fence, Quaternion.identity, block[block_num].transform);
						}
					}

				}
			}
		}
	}
	
	//destroy a block
	void delete(int block_num)
	{
		
		foreach (Transform child in block[block_num])
		{
			GameObject.Destroy(child.gameObject);
		}
		
	}
	
	//limit the digit from 1-9
	int mod(int D)
	{
		int remain = D;

		if (remain < 1) remain += 9;
		
		if (remain > 9) remain -= 9; 
		
		return remain;
	}
	
	//decide the road position
	void road_position(int rc, int length)	//length=length of the array rc=[row 1~3, column 1~3]
	{
		bool[] arr = new bool[length];
		
		for (int i = 0; i < length; i++)
		{
			r = Random.Range(0, 9);
			if (r > 7) arr[i] = true;
			else arr[i] = false;
		}

		if (rc == 1) row1 = arr;
		else if (rc == 2) row2 = arr;
		else if (rc == 3) row3 = arr;
		else if (rc == 4) column1 = arr;
		else if (rc == 5) column2 = arr;
		else column3 = arr;
	}
	
	//create map
	void map_produce(int block_num)
	{
		bool[] row = new bool[mapwidth];
		bool[] column = new bool[mapheight];

		if (block_num % 3 == 0) row = row1;
		else if (block_num % 3 == 1) row = row2;
		else row = row3;

		if (block_num <= 2) column = column1;
		else if (block_num <= 5) column = column2;
		else column = column3;
		
		for (widthi = 0; widthi < mapwidth; widthi++)
		{
			for (heighti = 0; heighti < mapheight; heighti++)
			{
				maps[widthi, heighti] = 0;
				if (row[widthi] == true) maps[widthi, heighti] += 5;
				if (column[heighti] == true) maps[widthi, heighti] += 3;
			}
		}
		
	}
	
	// Use this for initialization
	void Start()
	{
		row1 = new bool[mapwidth];
		row2 = new bool[mapwidth];
		row3 = new bool[mapwidth];
		column1 = new bool[mapheight];
		column2 = new bool[mapheight];
		column3 = new bool[mapheight];
		
		//structure of map
		road_position(1, mapwidth);
		road_position(2, mapwidth);
		road_position(3, mapwidth);
		road_position(4, mapheight);
		road_position(5, mapheight);
		road_position(6, mapheight);
		
		//adjust the block in the middle
		row2[4] = true;
		
		//build the maps and each block
		maps = new int[mapwidth, mapheight];
		
		//block 1
		map_produce(0);
		build(0, -mapwidth*obSize, mapheight*obSize);
		
		//block 2
		map_produce(1);
		build(1, 0, mapheight*obSize);

		//block 3
		map_produce(2);
		build(2, mapwidth*obSize, mapheight*obSize);

		//block 4
		map_produce(3);
		build(3, -mapwidth*obSize, 0);
		
		//block 5
		map_produce(4);
		build(4, 0, 0);

		//block 6
		map_produce(5);
		build(5, mapwidth*obSize, 0);
		
		//block 7
		map_produce(6);
		build(6, -mapwidth*obSize, -mapheight*obSize);
		
		//block 8
		map_produce(7);
		build(7, 0, -mapheight*obSize);
		
		//block 9
		map_produce(8);
		build(8, mapwidth*obSize, -mapheight*obSize);

		//mark the border of the map
		border_r = 2 * mapwidth * obSize;
		border_l = -mapwidth * obSize;
		border_u = 2 * mapheight * obSize;
		border_b = -mapheight * obSize;
		location = 5;
		
		Vector3 camera_position = GameObject.Find("Camera").transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Cursor.visible = false;
		
		//detect the position of the camera
		Vector3 camera_position = GameObject.Find("Camera").transform.position;
		int destr0, destr1, destr2;
		
		//adjust the maps
		if (Mathf.Abs(camera_position.x - border_r) < mapwidth * obSize)	//move to the right
		{
			//print("right");
			if (location % 3 == 0)
			{
				location -= 2;
				destr1 = location + 1;
				road_position(2, mapwidth);
			}
			else if (location % 3 == 2)
			{
				location += 1;
				destr1 = location - 2;
				road_position(1, mapwidth);
			}
			else //location % 3 == 1
			{
				location += 1;
				destr1 = location + 1;
				road_position(3, mapwidth);
			}
			destr0 = mod(destr1 - 3);
			destr2 = mod(destr1 + 3);

			//destroy left hand side, create right hand side
			delete(destr0 - 1);
			map_produce(destr0 - 1);
			build(destr0 - 1, border_r, border_b + 2 * mapheight * obSize);
			
			delete(destr1-1);
			map_produce(destr1 - 1);
			build(destr1 - 1, border_r, border_b + mapheight * obSize);
			
			delete(destr2-1);
			map_produce(destr2 - 1);
			build(destr2 - 1, border_r, border_b);
			
			//change border
			border_l += mapwidth * obSize;
			border_r += mapwidth * obSize;

		}
		else if (Mathf.Abs(camera_position.x - border_l) < mapwidth * obSize)	//extend to the left hand side
		{
			//print("left");
			if (location % 3 == 0)
			{
				location -= 1;
				destr1 = location - 1;
				road_position(1, mapwidth);
			}
			else if (location % 3 == 2)
			{
				location -= 1;
				destr1 = location + 2;
				road_position(3, mapwidth);
			}
			else //location % 3 == 1
			{
				location += 2;
				destr1 = location - 1;
				road_position(2, mapwidth);
			}
			destr0 = mod(destr1 - 3);
			destr2 = mod(destr1 + 3);

			//destroy left hand side, create right hand side
			delete(destr0 - 1);
			map_produce(destr0 - 1);
			build(destr0 - 1, border_l - mapwidth * obSize, border_b + 2 * mapheight * obSize);

			delete(destr1 - 1);
			map_produce(destr1 - 1);
			build(destr1 - 1, border_l - mapwidth * obSize, border_b + mapheight * obSize);

			delete(destr2 - 1);
			map_produce(destr2 - 1);
			build(destr2 - 1, border_l - mapwidth * obSize, border_b);
			
			//change border
			border_l -= mapwidth * obSize;
			border_r -= mapwidth * obSize;
			
			//if (location % 3 == 1) location += 2;
			//else location -= 1;
		}
		else if (Mathf.Abs(camera_position.z - border_u) < mapheight * obSize)	//up
		{
			location = mod(location - 3);
			destr1 = mod(location - 3);
			if (destr1 % 3 == 1)
			{
				destr0 = destr1 + 2;
				destr2 = destr1 + 1;
			}
			else if (destr1 % 3 == 0)
			{
				destr0 = destr1 - 1;
				destr2 = destr1 - 2;
			}
			else //destr1 % 3 == 2
			{
				destr0 = destr1 - 1;
				destr2 = destr1 + 1;
			}
			
			//decide the road position
			if (destr1 >= 7) road_position(6, mapheight);
			else if (destr1 >= 4) road_position(5, mapheight);
			else road_position(4, mapheight);

			//destroy left hand side, create right hand side
			delete(destr0 - 1);
			map_produce(destr0 - 1);
			build(destr0 - 1, border_l, border_u);

			delete(destr1 - 1);
			map_produce(destr1 - 1);
			build(destr1 - 1, border_l + mapwidth * obSize, border_u);

			delete(destr2 - 1);
			map_produce(destr2 - 1);
			build(destr2 - 1, border_l + 2 * mapwidth * obSize, border_u);
			
			//change border
			border_u += mapheight * obSize;
			border_b += mapheight * obSize;
		}
		else if (Mathf.Abs(camera_position.z - border_b) < mapheight * obSize)	//bottom
		{
			location = mod(location + 3);
			destr1 = mod(location + 3);
			if (destr1 % 3 == 1)
			{
				destr0 = destr1 + 2;
				destr2 = destr1 + 1;
			}
			else if (destr1 % 3 == 0)
			{
				destr0 = destr1 - 1;
				destr2 = destr1 - 2;
			}
			else //destr1 % 3 == 2
			{
				destr0 = destr1 - 1;
				destr2 = destr1 + 1;
			}

			//decide the road position
			if (destr1 >= 7) road_position(6, mapheight);
			else if (destr1 >= 4) road_position(5, mapheight);
			else road_position(4, mapheight);
			
			//destroy left hand side, create right hand side
			delete(destr0 - 1);
			map_produce(destr0 - 1);
			build(destr0 - 1, border_l, border_b - mapheight * obSize);

			delete(destr1 - 1);
			map_produce(destr1 - 1);
			build(destr1 - 1, border_l + mapwidth * obSize, border_b - mapheight * obSize);

			delete(destr2 - 1);
			map_produce(destr2 - 1);
			build(destr2 - 1, border_l + 2 * mapwidth * obSize, border_b - mapheight * obSize);
			
			//change border
			border_u -= mapheight * obSize;
			border_b -= mapheight * obSize;
		}
	}
	
}
