﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_view : MonoBehaviour
{
	public float movingspeed = 20.0f;

	// Use this for initialization
	void Start ()
	{
		transform.position = new Vector3(120, 3, 125);
	}
		
	// Update is called once per frame
	void Update ()
	{
		//use mouse input to rotate the camera
		float x = (Input.mousePosition.x - 700.0f)/8.0f;
		if (Mathf.Abs(x) > 5)
		{
			Vector3 rot = (Vector3.up * x * Time.deltaTime);
			transform.Rotate(rot);
		}

		//print(Input.mousePosition.x);
		//print(transform.forward);
		
		//use keyboard input to move the camera
		if (Input.GetKey(KeyCode.W)) transform.Translate(Vector3.forward * movingspeed * Time.deltaTime);
		if (Input.GetKey(KeyCode.S)) transform.Translate(Vector3.back * movingspeed * Time.deltaTime);
		if (Input.GetKey(KeyCode.D)) transform.Translate(Vector3.right * movingspeed * Time.deltaTime);
		if (Input.GetKey(KeyCode.A)) transform.Translate(Vector3.left * movingspeed * Time.deltaTime);

	}
}
