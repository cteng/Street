## Overview
- use `Unity` with `C#` to achieve procedural generation
- create a city with random houses, road alignment and some street props 
- the map will automatically extend as player moving so that player will always walk in the city
<br>

## Theory and Algorithm

### functions

#### create the map structure
- use boolean arrays to store the informations

>
![alt text](image/mapstructure.png)
>

- take corresponding arrays to create the map

>>
- take block4 as example
- there are two random boolean arrays row1 and column2
- then each of the small blocks inside the block4 will be assigned a value according to the row1 and column2
>>
>
![alt text](image/block4.png)
>
<br>

#### build the city according to the map
- roads

>
- 8: cross roads
- 5: vertical direction roads
- 3: horizontal direction roads
>

- street props

>
- put two street lamps on  all the vertical direction roads and horizontal direction roads
- randomly put the hydrants, benches, mail boxes and other things along the streets
- put the traffic light on the cross roads
>

- houses

>
- divide all the small blocks into three categories 
	1. next to the roads (normal houses)
	2. next to the cross road (telephone booth, houses with advertising board)
	3. far away from the roads (do not need to place the houses)
>

>
- place the houses randomly and adjust their direction according to the position of the small blocks
>

---

### initialization
- produce nine blocks and put the camera(first person view) on the middle part
- mark the position of player (block number)
- mark the borders
<br>
- red point as player position, yellow line as four borders, player position: 5

>
![alt text](image/initialization.png)
>

---

### update
- detect the distance between player and borders
- remove the blocks in another side and create new blocks
- update the position of player (block number)
- update the borders
<br>
- player position: 5

>
![alt text](image/update_1.png)
>

<br>
- player position: 6

>
![alt text](image/update_2.png)
>

<br>
- player position: 6, remove block1, block4 and block7

>
![alt text](image/update_3.png)
>

<br>
- player position: 6, extend the map and update the borders 

>
![alt text](image/update_4.png)
>

<br>

## Result

### achievement

- the map will detect the position of player and extend automatically
- all the houses are put in an appropriate way
- all the objects in the scene are reasonable

### player view

![alt text](image/view_1.png)

![alt text](image/view_2.png)

![alt text](image/view_3.png)

<br>

## Compile/start the game
- with UI interface

>
- download the zip file `Street`from the gitlab
- extract the file from the zip file
- run the file `city.x86_64` 
>

<br>

- with Terminal

```
git clone https://www.graphics.rwth-aachen.de:9000/cteng/Street
cd Street
./city.x86_64
```

if the file `city.x86_64` is not executable, then

```
chmod +x city.x86_64
./city.x86_64
```

<br>

## Manipulation
- first person controller
- use mouse to adjust the view direction
- use keys `W` `S` `A` `D` on keyboard to move the player

>
`W` moving forward<br>
 `S` moving backward<br>
 `A ` moving to the left hand side<br>
 `D` moving to the right hand side
  >
  
  <br>
  
## Reference
- Unity Scripting Reference https://docs.unity3d.com/ScriptReference/